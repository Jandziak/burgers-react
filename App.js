import React, { useState } from "react";
import {
  NavigationContainer,
  DarkTheme as NavigationDarkTheme,
  DefaultTheme as NavigationDefaultTheme
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

import HomeScreen from "./components/HomeScreen";
import DetailsScreen from "./components/DetailsScreen";
import AddBurger from "./components/AddBurger";
import {
  Provider as PaperProvider,
  DarkTheme as PaperDarkTheme,
  DefaultTheme as PaperDefaultTheme
} from "react-native-paper";
import { Header } from "./components/Header";
const client = new ApolloClient({
  uri: "https://api.graph.cool/simple/v1/ck6i38j6n2r0e01072mflysrc",
  cache: new InMemoryCache({
    cacheRedirects: {
      Query: {
        Burger: (_, args, { getCacheKey }) =>
          getCacheKey({ __typename: "Burger", id: args.id })
      }
    }
  })
});

const Stack = createStackNavigator();

function App() {
  const CombinedDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme
  };
  const CombinedDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme
  };
  const [darkTheme, setDarkTheme] = useState(false);
  const theme = darkTheme ? CombinedDarkTheme : CombinedDefaultTheme;
  function toggleTheme() {
    // We will pass this function to Drawer and invoke it on theme switch press
    setDarkTheme(isDark => !isDark);
  }
  return (
    <ApolloProvider client={client}>
      <PaperProvider theme={theme}>
        <NavigationContainer theme={theme}>
          <Stack.Navigator
            initialRouteName="My Foods"
            screenOptions={{
              header: ({ scene, previous, navigation }) => (
                <Header
                  scene={scene}
                  previous={previous}
                  navigation={navigation}
                  toggleTheme={toggleTheme}
                />
              )
            }}
          >
            <Stack.Screen
              name="My Foods"
              options={{
                title: "My food",
                button: {
                  icon: "plus",
                  onPress: navigation => navigation.push("AddBurger")
                }
              }}
              component={HomeScreen}
            />
            <Stack.Screen name="Details" component={DetailsScreen} />
            <Stack.Screen
              name="AddBurger"
              options={{ title: "Add Burger" }}
              component={AddBurger}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    </ApolloProvider>
  );
}

export default App;
