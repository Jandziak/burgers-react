import React, { useState } from "react";
import { ScrollView } from "react-native";

import { useNavigation } from "@react-navigation/native";
import { useMutation } from "@apollo/react-hooks";
import { _pickImage } from "./ImagePicker";
import {
  Card,
  Title,
  TextInput,
  Paragraph,
  IconButton
} from "react-native-paper";
import { ADD_BURGER, ALL_BURGERS_QUERY } from "./graphquery";

export default function AddBurgerCard({ uri }) {
  const [name, setName] = useState("Burger Name");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState(null);

  const [burger] = useMutation(ADD_BURGER);
  const navigation = useNavigation();

  return (
    <ScrollView>
      <Card>
        <Card.Content>
          <Title> {name} </Title>
          <Paragraph>{description}</Paragraph>
          <Card.Cover source={{ uri: image ? image : uri }} />
          <TextInput
            label="Burger Name"
            placeholder="type burger name"
            onChangeText={text => setName(text)}
          />
          <TextInput
            label="Burger Description"
            placeholder="type burger description"
            style={{ height: 100 }}
            multiline={true}
            onChangeText={text => setDescription(text)}
          />
        </Card.Content>
        <Card.Actions>
          <IconButton
            icon="camera"
            size={40}
            onPress={async () => {
              const image = await _pickImage();
              setImage(image);
            }}
          />
          <IconButton
            icon="plus"
            size={40}
            onPress={() => {
              burger({
                variables: { uri: uri, desc: description, name: name },
                optimisticResponse: {
                  __typename: "Mutation",
                  createBurger: {
                    id: "1",
                    name: name,
                    description: description,
                    image: uri,
                    __typename: "Burger"
                  }
                },
                update: (proxy, { data: { createBurger } }) => {
                  // Read the data from our cache for this query.
                  const data = proxy.readQuery({ query: ALL_BURGERS_QUERY });
                  // Write our data back to the cache with the new comment in it
                  proxy.writeQuery({
                    query: ALL_BURGERS_QUERY,
                    data: {
                      burgers: [...data.burgers, createBurger]
                    }
                  });
                }
              });
              navigation.navigate("My Foods");
            }}
          />
        </Card.Actions>
      </Card>
    </ScrollView>
  );
}
