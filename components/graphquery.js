import { gql } from "apollo-boost";
const fragments = {
  burgers: gql`
    fragment Burgers on Burger {
      image: imageUrl
      description
      name
      id
    }
  `
};
export const ADD_BURGER = gql`
  mutation($uri: String!, $desc: String!, $name: String!) {
    createBurger(description: $desc, name: $name, imageUrl: $uri) {
      ...Burgers
    }
  }
  ${fragments.burgers}
`;

export const ALL_BURGERS_QUERY = gql`
  query burgers {
    burgers: allBurgers {
      ...Burgers
    }
  }
  ${fragments.burgers}
`;

export const BURGER_DETAIL_QUERY = gql`
  query getBurger($id: ID!) {
    burger: Burger(id: $id) {
      ...Burgers
    }
  }
  ${fragments.burgers}
`;

export const DELETE_BURGER = gql`
  mutation($id: ID!) {
    deleteBurger(id: $id) {
      id
    }
  }
`;
