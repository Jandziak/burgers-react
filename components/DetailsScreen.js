import React from "react";
import { SafeAreaView, ScrollView, ActivityIndicator } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";
import { useQuery, useMutation } from "@apollo/react-hooks";

import { styles } from "../styles/styles";
import { Card, Avatar, Title, Button, Paragraph } from "react-native-paper";

import IngradientList from "../components/IngradientList";
import {
  BURGER_DETAIL_QUERY,
  ALL_BURGERS_QUERY,
  DELETE_BURGER
} from "./graphquery";

export default function DetailsScreen() {
  const route = useRoute();
  const navigation = useNavigation();
  const { loading, data } = useQuery(BURGER_DETAIL_QUERY, {
    variables: { id: route.params?.id },
    fetchPolicy: "cache-and-network"
  });

  const [deleteBurger] = useMutation(DELETE_BURGER);

  const deleteBurgerOnPress = () => {
    deleteBurger({
      variables: {
        id: route.params?.id
      },
      optimisticResponse: {
        __typename: "Mutation",
        deleteBurger: {
          id: route.params?.id,
          __typename: "Burger"
        }
      },
      update: proxy => {
        // Read the data from our cache for this query.
        const data = proxy.readQuery({ query: ALL_BURGERS_QUERY });
        // Write our data back to the cache with the new comment in it
        const newData = data.burgers.filter(
          elem => elem.id != route.params?.id
        );
        proxy.writeQuery({
          query: ALL_BURGERS_QUERY,
          data: {
            burgers: newData
          }
        });
      }
    });

    navigation.navigate("My Foods");
  };

  if (loading & !data) return <ActivityIndicator size="large" />;
  navigation.setOptions({
    title: data.burger.name,
    button: {
      icon: "delete",
      onPress: deleteBurgerOnPress
    }
  });
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <Card>
          <Card.Title
            title="Burgers"
            subtitle="Vege burgers"
            left={props => <Avatar.Icon {...props} icon="folder" />}
          />
          <Card.Content>
            <Title> {data.burger.name} </Title>
            <Card.Cover source={{ uri: data.burger.image }} />
            <Paragraph>{data.burger.description}</Paragraph>
          </Card.Content>
          <Card.Actions>
            <Button
              title="Order"
              onPress={() => alert("You tapped the button!")}
            >
              Order
            </Button>
            <Button title="Delete Burger" onPress={deleteBurgerOnPress}>
              Delete Burger
            </Button>
          </Card.Actions>
          <IngradientList />
        </Card>
      </ScrollView>
    </SafeAreaView>
  );
}
