import React from "react";
import { TouchableOpacity } from "react-native";
import { Appbar, Avatar } from "react-native-paper";

export const Header = ({ scene, previous, navigation, toggleTheme }) => {
  const { options } = scene.descriptor;
  const title = options.title;
  return (
    <Appbar.Header>
      {previous ? (
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      ) : (
        <TouchableOpacity
          onPress={() => {
            toggleTheme();
          }}
        >
          <Avatar.Image
            size={40}
            source={{
              uri:
                "https://media-exp1.licdn.com/dms/image/C5103AQFPe1smYLmDNA/profile-displayphoto-shrink_200_200/0?e=1590019200&v=beta&t=QPyRlIbvgRxabP4SqYyVvlrVm1qVbLXVOTj-s9UZYhY"
            }}
          />
        </TouchableOpacity>
      )}
      <Appbar.Content title={title} />
      {options.button ? (
        <Appbar.Action
          icon={options.button.icon}
          size={40}
          onPress={() => options.button.onPress(navigation)}
        />
      ) : null}
    </Appbar.Header>
  );
};
