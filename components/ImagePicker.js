import React, { useState, useEffect } from "react";
import { Button, Image, View } from "react-native";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";

import { styles } from "../styles/styles";

async function getPermissionAsync() {
  if (Constants.platform.ios) {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status !== "granted") {
      alert("Sorry, we need camera roll permissions to make this work!");
    }
  }
}

export async function _pickImage() {
  let result = await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: true,
    aspect: [4, 3],
    quality: 1
  });
  console.log(result);
  if (!result.cancelled) {
    return result.uri;
  }
  return null;
}

export default function ImagePickerExample() {
  const [image, setImage] = useState(null);

  useEffect(() => {
    getPermissionAsync();
  }, []);

  return (
    <View style={(styles.container, styles.view)}>
      <Button
        title="Pick an image from camera roll"
        onPress={async () => {
          const image = await _pickImage();
          setImage(image);
        }}
      />
      {image && <Image source={{ uri: image }} style={styles.imageDetails} />}
    </View>
  );
}
