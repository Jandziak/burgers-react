import * as React from "react";
import { TouchableOpacity } from "react-native";

import { Card, Title } from "react-native-paper";
export default function Item({ title, uri, onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Card>
        <Card.Content>
          <Title> {title} </Title>
          <Card.Cover source={{ uri: uri }} />
        </Card.Content>
      </Card>
    </TouchableOpacity>
  );
}
