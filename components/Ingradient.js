import * as React from "react";
import { View } from "react-native";

import { Card, Title } from "react-native-paper";
export default function Item({ title, uri }) {
  return (
    <View>
      <Card style={{ width: 250 }}>
        <Card.Content>
          <Title> {title} </Title>
          <Card.Cover source={{ uri: uri }} />
        </Card.Content>
      </Card>
    </View>
  );
}
