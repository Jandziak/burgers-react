import React from "react";
import {
  Text,
  View,
  SafeAreaView,
  FlatList,
  ActivityIndicator
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useQuery } from "@apollo/react-hooks";

import Item from "./Item";
import { styles } from "../styles/styles";

import { ALL_BURGERS_QUERY } from "./graphquery";

export default function HomeScreen() {
  const navigation = useNavigation();
  const { loading, error, data } = useQuery(ALL_BURGERS_QUERY, {
    fetchPolicy: "network-only"
  });
  if (loading) return <ActivityIndicator size="large" />;
  if (error)
    return (
      <View>
        <Text>Err </Text>
      </View>
    );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data.burgers}
        renderItem={({ item }) => (
          <Item
            title={item.name}
            uri={item.image}
            onPress={() =>
              navigation.push("Details", {
                title: item.name,
                id: item.id
              })
            }
          />
        )}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}
