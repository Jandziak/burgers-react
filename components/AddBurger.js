import React, { useState } from "react";
import { Text, TextInput, ScrollView, Image, Button } from "react-native";
import { useMutation } from "@apollo/react-hooks";

import { useNavigation } from "@react-navigation/native";
import { styles } from "../styles/styles";
import ImagePicker from "./ImagePicker";
import AddBurgerCard from "./AddBurgerCard";
const CUSTOM_LINK =
  "https://image.cnbcfm.com/api/v1/image/106152658-1569589928424pltstill-twitterlinkedin.jpg?v=1569589970&w=678&h=381";

import { ADD_BURGER, ALL_BURGERS_QUERY } from "./graphquery";

export default function AddBurger() {
  const [title, setName] = useState("");
  const [description, setDescription] = useState("");
  const [showImage, setShowImage] = useState(true);
  const [burger] = useMutation(ADD_BURGER);
  const navigation = useNavigation();
  return (
    <ScrollView style={styles.container}>
      <AddBurgerCard title="Burger Name" uri={CUSTOM_LINK} />
    </ScrollView>
  );
}
