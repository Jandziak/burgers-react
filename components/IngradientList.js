import React from "react";
import { View, FlatList } from "react-native";
import { Title } from "react-native-paper";
import Ingradient from "./Ingradient";
const skladniki = require("../skladniki.json");
export default function IngradientList() {
  return (
    <View style={{ margin: 30, height: 220, flex: 1 }}>
      <Title>Ingradient List</Title>
      <FlatList
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        data={skladniki}
        renderItem={({ item }) => (
          <Ingradient title={item.name} uri={item.image} />
        )}
        keyExtractor={item => `${item.id}`}
      />
    </View>
  );
}
