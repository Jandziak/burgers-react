import { StyleSheet, Dimensions } from "react-native";

const width = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  imageHome: {
    width: width,
    height: 200
  },
  imageDetails: {
    width: width,
    height: 200
  },
  textDetails: {
    margin: 10,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  textItem: {
    margin: 10,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  container: {
    flex: 1
  },
  view: {
    alignItems: "center",
    justifyContent: "center"
  },
  textFont: {
    fontSize: 20,
    textAlign: "center"
  }
});
